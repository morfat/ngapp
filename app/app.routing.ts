import { ModuleWithProviders } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemListComponent } from './items/item-list.component';
//import { ItemDetailComponent } from './items/item-detail.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './_guards/auth.guard';

const appRoutes: Routes=[
{ path:'login',
  component: LoginComponent
  },

{
        
        path:'dashboard',
        component:DashboardComponent,
        canActivate:[AuthGuard]
        }
    ,
    {
        path:'',
        redirectTo:'/dashboard',
        pathMatch:'full'
        },
    {
        path:'items',
        component:ItemListComponent,
        canActivate:[AuthGuard]
        }
    
    ];

export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);
