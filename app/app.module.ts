import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { HttpModule} from '@angular/http';


//import './rxjs-extensions';
import {FormsModule} from '@angular/forms';

import { AuthGuard } from './_guards/auth.guard';
import { AuthenticationService } from './_services/authentication.service';
import { LoginComponent } from './login/login.component';


import {AppComponent} from './app.component';
import {ItemListComponent} from './items/item-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {routing} from './app.routing';


@NgModule({
	imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
    ],
    
	declarations:[
    AppComponent,
    DashboardComponent,
    ItemListComponent,
    LoginComponent
    ],
    providers: [
    AuthGuard,
    AuthenticationService
    ],
    
	bootstrap: [AppComponent]
})
export class AppModule{}

