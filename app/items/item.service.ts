import {Injectable} from '@angular/core';
import {Http,Response,Headers,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import {Item} from './item';
import { AuthenticationService } from '../_services/authentication.service';


//import {Observable} from 'rxjs';

@Injectable()
export class ItemService {
        
    constructor(private http:Http,
    private authenticationService: AuthenticationService){}
    
    
    private itemsUrl='http://localhost:8000/items/';
    private token: string;
    
    getItems():Observable<Item[]>{
        //auth header
        console.log(this.authenticationService.token);
        
        let headers=new Headers({'Authorization': 'Token '+ this.authenticationService.token});
        
        let options=new RequestOptions({headers:headers});
        
        
        return this.http.get(this.itemsUrl,options).map(
        this.extractData).catch(this.handleError)
        }
    
    getItemsViaLink(link:string):Observable<Item[]>{
         let headers=new Headers({'Authorization': 'Token '+ this.authenticationService.token});
        
        let options=new RequestOptions({headers:headers});
        
        
        return this.http.get(link,options).map(
        this.extractData).catch(this.handleError)
        }
    
    
    addItem(name:string,description:string,stored_location:string):Observable<Item>{
        let body=JSON.stringify({name,description,stored_location});
        let headers=new Headers({'Content-Type':'application/json',
                                   'Authorization': 'Token '+ this.authenticationService.token
                                });
        
        
        let options=new RequestOptions({headers:headers});
        
        return this.http.post(this.itemsUrl,body,options).map(
        this.extractData).catch(this.handleError);
        }
    
    updateItem(id:number,name:string,description:string,stored_location:string):Observable<Item>{
        let updateUrl=`${this.itemsUrl}${id}/`;
        
        let body=JSON.stringify({id,name,description,stored_location});
        //let headers=new Headers({'Content-Type':'application/json'});
        
        let headers=new Headers({'Content-Type':'application/json',
                                   'Authorization': 'Token '+ this.authenticationService.token
                                });
        
        let options=new RequestOptions({headers:headers});
        
        return this.http.put(updateUrl,body,options).map(
        this.extractData).catch(this.handleError);
        }
    
    deleteItem(item:Item):Observable<void>{
        let deleteUrl=`${this.itemsUrl}${item.id}/`;
        
      
        //let headers=new Headers({'Content-Type':'application/json'});
         let headers=new Headers({'Content-Type':'application/json',
                                   'Authorization': 'Token '+ this.authenticationService.token
                                });
        
        let options=new RequestOptions({headers:headers});
        
        return this.http.delete(deleteUrl,options).catch(this.handleError);
        }
    
    searchItem(term:string): Observable<Item[]>{
          let headers=new Headers({'Authorization': 'Token '+ this.authenticationService.token
                                });
        let options=new RequestOptions({headers:headers});
        
        return this.http.get(`${this.itemsUrl}?name=${term}`,options).map(
        (r:Response)=>r.json() as Item[]);
        }
    
    
    
    private extractData(res: Response){
        let body=res.json();
        return body || {};
        
        }
    
    private handleError(error:any){
        let errMsg=(error.message)? error.message:error.status ?
        `${error.status}-${error.statusText}`: 'Server Error';
        console.error(errMsg); //log to console instead
        return Observable.throw(errMsg);
       }
    
    }
