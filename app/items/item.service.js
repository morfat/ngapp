"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
var authentication_service_1 = require('../_services/authentication.service');
//import {Observable} from 'rxjs';
var ItemService = (function () {
    function ItemService(http, authenticationService) {
        this.http = http;
        this.authenticationService = authenticationService;
        this.itemsUrl = 'http://localhost:8000/items/';
    }
    ItemService.prototype.getItems = function () {
        //auth header
        console.log(this.authenticationService.token);
        var headers = new http_1.Headers({ 'Authorization': 'Token ' + this.authenticationService.token });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get(this.itemsUrl, options).map(this.extractData).catch(this.handleError);
    };
    ItemService.prototype.getItemsViaLink = function (link) {
        var headers = new http_1.Headers({ 'Authorization': 'Token ' + this.authenticationService.token });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get(link, options).map(this.extractData).catch(this.handleError);
    };
    ItemService.prototype.addItem = function (name, description, stored_location) {
        var body = JSON.stringify({ name: name, description: description, stored_location: stored_location });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json',
            'Authorization': 'Token ' + this.authenticationService.token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.itemsUrl, body, options).map(this.extractData).catch(this.handleError);
    };
    ItemService.prototype.updateItem = function (id, name, description, stored_location) {
        var updateUrl = "" + this.itemsUrl + id + "/";
        var body = JSON.stringify({ id: id, name: name, description: description, stored_location: stored_location });
        //let headers=new Headers({'Content-Type':'application/json'});
        var headers = new http_1.Headers({ 'Content-Type': 'application/json',
            'Authorization': 'Token ' + this.authenticationService.token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.put(updateUrl, body, options).map(this.extractData).catch(this.handleError);
    };
    ItemService.prototype.deleteItem = function (item) {
        var deleteUrl = "" + this.itemsUrl + item.id + "/";
        //let headers=new Headers({'Content-Type':'application/json'});
        var headers = new http_1.Headers({ 'Content-Type': 'application/json',
            'Authorization': 'Token ' + this.authenticationService.token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.delete(deleteUrl, options).catch(this.handleError);
    };
    ItemService.prototype.searchItem = function (term) {
        var headers = new http_1.Headers({ 'Authorization': 'Token ' + this.authenticationService.token
        });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get(this.itemsUrl + "?name=" + term, options).map(function (r) { return r.json(); });
    };
    ItemService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    ItemService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message : error.status ?
            error.status + "-" + error.statusText : 'Server Error';
        console.error(errMsg); //log to console instead
        return Observable_1.Observable.throw(errMsg);
    };
    ItemService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, authentication_service_1.AuthenticationService])
    ], ItemService);
    return ItemService;
}());
exports.ItemService = ItemService;
//# sourceMappingURL=item.service.js.map