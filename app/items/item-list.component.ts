import {Component,OnInit } from '@angular/core';

//Add the RxJS Observable operators we need in this app
import 'app/rxjs-operators';
import {Item} from './item';
import {ItemService} from './item.service';
import { Subject }          from 'rxjs/Subject';

declare var jQuery:any;


@Component({
    selector:'item-list',
    styles:[`.selected {
    background-color: #337ab7; !important;
    color: white;
  }`],
    templateUrl:'app/items/item-list.template.html',
    providers:[ItemService]
    })
export class ItemListComponent implements OnInit {
        
   items:Item[];
   previous_link=null;
   next_link=null;
   total_results=null;
     
   errorMessage: string;
   
   item=new Item();
   itemCreateFormSubmitted=false;
   
   
   formActive=true; //for toggle form in reset.
   selectedItem=null;
    
   onPaginationNextLink(){
       //get next data
        
       this.itemService.getItemsViaLink(this.next_link).subscribe(
       response=>(this.items=response.results,
       this.previous_link=response.previous,
       this.next_link=response.next,
       this.total_results=response.count),
       error=> this.errorMessage=<any>error);
   
       
       return false;
       
       }
    
    onPaginationPreviousLink(){
       //get next data
        
       this.itemService.getItemsViaLink(this.previous_link).subscribe(
       response=>(this.items=response.results,
       this.previous_link=response.previous,
       this.next_link=response.next,
       this.total_results=response.count),
       error=> this.errorMessage=<any>error);
   
       
       return false;
       
       }
    
   onSelectItem(item: Item){
       this.selectedItem=item;
       }
   
    
    resetItemForm(){
        this.item=new Item();
        this.formActive=false;
        setTimeout(()=>this.formActive=true,0);
        }
    
   
   constructor(private itemService:ItemService){}
   
    showModal(){
        jQuery("#myModal").modal("show");
        }
    
    showEditModal(){
        if (this.selectedItem){
            jQuery("#myEditModal").modal("show");
            }
        else{
            alert("Nothing selected");
            }
        
        }
    showDeleteModal(){
        if (this.selectedItem){
            jQuery("#myDeleteModal").modal("show");
            }
        else{
            alert("Nothing selected");
            }
        
        }
    
    onCancelEdit(){
        //reset selected item
        this.selectedItem=null;
        
        //close modal
        jQuery("#myEditModal").modal("hide");
        }
    
     onCancelDelete(){
        
        
        //close modal
        jQuery("#myDeleteModal").modal("hide");
         //reset selected item
        this.selectedItem=null;
        }
    
    private searchTermStream = new Subject<string>();
    
    search(term: string) { this.searchTermStream.next(term);
    
     this.searchTermStream
    .debounceTime(300)
    .distinctUntilChanged()
    .switchMap(
        (term: string) => this.itemService.searchItem(term)).subscribe(
       response=>(this.items=response.results,
       this.previous_link=response.previous,
       this.next_link=response.next,
       this.total_results=response.count),
       error=> this.errorMessage=<any>error);
    
    }
    
    
    
    ngOnInit(){
        
        this.getItems();
    
        
    }
    
    onSubmit(){
        this.itemCreateFormSubmitted=true;
        
        //save modal to db
        this.itemService.addItem(this.item.name,this.item.description,
        this.item.stored_location).subscribe(
            item=>this.items.push(item),
            error=>this.errorMessage=<any>error);
        
        this.itemCreateFormSubmitted=true;
    
    }
    
    onSubmitEditForm(){
        //get selected item 
        console.log(this.selectedItem);
        
        this.itemService.updateItem(this.selectedItem.id,
        this.selectedItem.name,
        this.selectedItem.description,
        this.selectedItem.stored_location).subscribe(error=>this.errorMessage=<any>error);
        
        this.onCancelEdit();
        }
    
    
     onSubmitDeleteForm(){
        //delete selected item
        this.itemService.deleteItem(this.selectedItem).subscribe(
        ()=>this.getItems(),error=>this.errorMessage=<any>error);
        
        this.onCancelDelete();
        }
    
    
    
   getItems(){
           
       this.itemService.getItems().subscribe(
       response=>(this.items=response.results,
       this.previous_link=response.previous,
       this.next_link=response.next,
       this.total_results=response.count),
       error=> this.errorMessage=<any>error);
   }
    
 }