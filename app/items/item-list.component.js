"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
//Add the RxJS Observable operators we need in this app
require('app/rxjs-operators');
var item_1 = require('./item');
var item_service_1 = require('./item.service');
var Subject_1 = require('rxjs/Subject');
var ItemListComponent = (function () {
    function ItemListComponent(itemService) {
        this.itemService = itemService;
        this.previous_link = null;
        this.next_link = null;
        this.total_results = null;
        this.item = new item_1.Item();
        this.itemCreateFormSubmitted = false;
        this.formActive = true; //for toggle form in reset.
        this.selectedItem = null;
        this.searchTermStream = new Subject_1.Subject();
    }
    ItemListComponent.prototype.onPaginationNextLink = function () {
        //get next data
        var _this = this;
        this.itemService.getItemsViaLink(this.next_link).subscribe(function (response) { return (_this.items = response.results,
            _this.previous_link = response.previous,
            _this.next_link = response.next,
            _this.total_results = response.count); }, function (error) { return _this.errorMessage = error; });
        return false;
    };
    ItemListComponent.prototype.onPaginationPreviousLink = function () {
        //get next data
        var _this = this;
        this.itemService.getItemsViaLink(this.previous_link).subscribe(function (response) { return (_this.items = response.results,
            _this.previous_link = response.previous,
            _this.next_link = response.next,
            _this.total_results = response.count); }, function (error) { return _this.errorMessage = error; });
        return false;
    };
    ItemListComponent.prototype.onSelectItem = function (item) {
        this.selectedItem = item;
    };
    ItemListComponent.prototype.resetItemForm = function () {
        var _this = this;
        this.item = new item_1.Item();
        this.formActive = false;
        setTimeout(function () { return _this.formActive = true; }, 0);
    };
    ItemListComponent.prototype.showModal = function () {
        jQuery("#myModal").modal("show");
    };
    ItemListComponent.prototype.showEditModal = function () {
        if (this.selectedItem) {
            jQuery("#myEditModal").modal("show");
        }
        else {
            alert("Nothing selected");
        }
    };
    ItemListComponent.prototype.showDeleteModal = function () {
        if (this.selectedItem) {
            jQuery("#myDeleteModal").modal("show");
        }
        else {
            alert("Nothing selected");
        }
    };
    ItemListComponent.prototype.onCancelEdit = function () {
        //reset selected item
        this.selectedItem = null;
        //close modal
        jQuery("#myEditModal").modal("hide");
    };
    ItemListComponent.prototype.onCancelDelete = function () {
        //close modal
        jQuery("#myDeleteModal").modal("hide");
        //reset selected item
        this.selectedItem = null;
    };
    ItemListComponent.prototype.search = function (term) {
        var _this = this;
        this.searchTermStream.next(term);
        this.searchTermStream
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(function (term) { return _this.itemService.searchItem(term); }).subscribe(function (response) { return (_this.items = response.results,
            _this.previous_link = response.previous,
            _this.next_link = response.next,
            _this.total_results = response.count); }, function (error) { return _this.errorMessage = error; });
    };
    ItemListComponent.prototype.ngOnInit = function () {
        this.getItems();
    };
    ItemListComponent.prototype.onSubmit = function () {
        var _this = this;
        this.itemCreateFormSubmitted = true;
        //save modal to db
        this.itemService.addItem(this.item.name, this.item.description, this.item.stored_location).subscribe(function (item) { return _this.items.push(item); }, function (error) { return _this.errorMessage = error; });
        this.itemCreateFormSubmitted = true;
    };
    ItemListComponent.prototype.onSubmitEditForm = function () {
        var _this = this;
        //get selected item 
        console.log(this.selectedItem);
        this.itemService.updateItem(this.selectedItem.id, this.selectedItem.name, this.selectedItem.description, this.selectedItem.stored_location).subscribe(function (error) { return _this.errorMessage = error; });
        this.onCancelEdit();
    };
    ItemListComponent.prototype.onSubmitDeleteForm = function () {
        var _this = this;
        //delete selected item
        this.itemService.deleteItem(this.selectedItem).subscribe(function () { return _this.getItems(); }, function (error) { return _this.errorMessage = error; });
        this.onCancelDelete();
    };
    ItemListComponent.prototype.getItems = function () {
        var _this = this;
        this.itemService.getItems().subscribe(function (response) { return (_this.items = response.results,
            _this.previous_link = response.previous,
            _this.next_link = response.next,
            _this.total_results = response.count); }, function (error) { return _this.errorMessage = error; });
    };
    ItemListComponent = __decorate([
        core_1.Component({
            selector: 'item-list',
            styles: [".selected {\n    background-color: #337ab7; !important;\n    color: white;\n  }"],
            templateUrl: 'app/items/item-list.template.html',
            providers: [item_service_1.ItemService]
        }), 
        __metadata('design:paramtypes', [item_service_1.ItemService])
    ], ItemListComponent);
    return ItemListComponent;
}());
exports.ItemListComponent = ItemListComponent;
//# sourceMappingURL=item-list.component.js.map