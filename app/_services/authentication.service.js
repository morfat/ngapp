"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
//import {Observable} from 'rxjs';
require('rxjs/add/operator/map');
var Observable_1 = require('rxjs/Observable');
var AuthenticationService = (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.authUrl = 'http://localhost:8000/obtain-token/';
        //set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
    AuthenticationService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        var body = JSON.stringify({ username: username, password: password });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.authUrl, body, options).map(function (response) {
            //login successful if token in response
            var token = response.json() && response.json().token;
            if (token) {
                //set token property
                _this.token = token;
                //store username and token in local storage
                localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                //return true to indicate succesfull
                return true;
            }
            else {
                //return false to indicate failed login
                return false;
            }
        }).catch(this.handleError);
    };
    AuthenticationService.prototype.logout = function () {
        //clear token to remove user from local storage 
        this.token = null;
        localStorage.removeItem('currentUser');
    };
    AuthenticationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map