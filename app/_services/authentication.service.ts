import {Injectable} from '@angular/core';
import {Http,Headers,Response,RequestOptions} from '@angular/http';
//import {Observable} from 'rxjs';
import 'rxjs/add/operator/map'
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthenticationService{
        public token:string;
         
    private authUrl='http://localhost:8000/obtain-token/';
    
    
    constructor(private http:Http){
        //set token if saved in local storage
        var currentUser=JSON.parse(
        localStorage.getItem('currentUser'));
        this.token=currentUser && currentUser.token;
        }
    
    private handleError(error:any){
       return Observable.throw(error);
       }
    
    login(username,password):Observable<boolean>{
         let body=JSON.stringify({username, password});
        let headers=new Headers({'Content-Type':'application/json'});
        let options=new RequestOptions({headers:headers});
        
       
        return this.http.post(this.authUrl,body,options).map(
        
           (response:Response)=>{
            //login successful if token in response
            let token=response.json() && response.json().token;
            if (token){
                //set token property
                this.token=token;
                //store username and token in local storage
                localStorage.setItem('currentUser',JSON.stringify({ username: username, token: token }));
                //return true to indicate succesfull
                return true;
                }
            else{
                //return false to indicate failed login
                return false;
                }
            }).catch(this.handleError);
        }
    logout():void{
        //clear token to remove user from local storage 
        this.token=null;
        localStorage.removeItem('currentUser');
        }
    }

        