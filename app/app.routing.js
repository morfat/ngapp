"use strict";
var router_1 = require('@angular/router');
var dashboard_component_1 = require('./dashboard/dashboard.component');
var item_list_component_1 = require('./items/item-list.component');
//import { ItemDetailComponent } from './items/item-detail.component';
var login_component_1 = require('./login/login.component');
var auth_guard_1 = require('./_guards/auth.guard');
var appRoutes = [
    { path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'dashboard',
        component: dashboard_component_1.DashboardComponent,
        canActivate: [auth_guard_1.AuthGuard]
    },
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    },
    {
        path: 'items',
        component: item_list_component_1.ItemListComponent,
        canActivate: [auth_guard_1.AuthGuard]
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map